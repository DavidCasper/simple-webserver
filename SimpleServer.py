from flask import Flask,render_template
import requests
import plotly.plotly as py
import plotly.graph_objs as go

app = Flask(__name__,)
STATIC_AUTO_RELOAD=True
r = requests.get('https://titanic.businessoptics.biz/survival/')
passengers = r.json()
py.sign_in('xxxxx', 'zk58x8ic99')  # Replace the username, and API key with your credentials which can be found under
# settings in your plotly profile
@app.route('/')
def simpleServer():
    return render_template('Interface.html')

@app.route('/gender/')
def gender():

    #index 0 = male and index 1 = female
    tempcount = 0
    survive = [0] *2
    total = [0] * 2
    results = [0] * 2
    for i in passengers:
            if i["sex"] == "male":
                total[0] = total[0] + 1
                if i["survived"] == "1":
                    survive[0] = survive[0] + 1
            else:
                total[1] = total[1] + 1
                if i["survived"] == "1":
                    survive[1] = survive[1] + 1

    for i in survive:
        results[tempcount]=(survive[tempcount]/total[tempcount]*100)
        tempcount= tempcount+1

    output= "Female survival rate: " + str(survive[1]) + \
    "% " + '\n' + "Male survival rate: " + str(survive[0]) + "%"


    data = [go.Bar(
        x=['Male','Female'],
        y=results
    )]
    layout = go.Layout(title="SurvialRateSexBarGraph", xaxis={'title': 'Sex'}, yaxis={'title': 'Survival Rate %'})
    figure = go.Figure(data=data, layout=layout)
    py.plot(figure, filename='SurvialRateSexBarGraph')

    f = open('/home/david/PycharmProjects/SimpleServer/static/GenderResult.txt', 'w')
    f.write(output)
    f.close()

    return render_template('GenderResult.html')


@app.route('/age/')
def age():
    #Assuming highest age on the boat was 120
    survive = [0] * 121
    total = [0] * 121
    results=[0]*121
    ages = [0]*121
    tempcount = 0
    output = ""
    for i in passengers:
        current = i["age"]
        if current=="":
            continue
        #simplifies age by rounding off
        age = int(round(float(current)))
        total[age] = total[age] + 1
        if i["survived"] == "1":
            survive[age] = survive[age] + 1

    tracker = 0
    for j in total:
        if j!= 0:
            results[tempcount]=survive[tempcount] / total[tempcount] * 100
            ages[tempcount]=tempcount
            output = output+"Age: " + str(tempcount)+" survival rate: " + str(results[tempcount])
            output = output+ "\n"
            tracker = tracker+1
        tempcount = tempcount+1
    optimizedResults = [0] * (tracker+1)
    optimizedAges = [0] *(tracker+1)
    count = 0
    for k in optimizedAges:
        optimizedAges[count]=ages[count]
        optimizedResults[count]=results[count]
        count=count+1
    data = [go.Bar(
        x=optimizedAges,
        y=optimizedResults
    )]
    layout = go.Layout(title="SurvialRateAgeBarGraph", xaxis={'title': 'Age'}, yaxis={'title': 'Survival Rate %'})
    figure = go.Figure(data=data, layout=layout)
    py.plot(figure, filename='SurvialRateAgeBarGraph')


    f = open('/home/david/PycharmProjects/SimpleServer/static/AgeResult.txt', 'w')
    f.write(output)
    f.close()

    return render_template('AgeResult.html')


@app.route('/class/')
def Class():
        survive = [0]*3
        total = [0]*3
        result = [0]*3
        for i in passengers:
                current = i["class"]
                #uses class number to find the index in the array
                total[int(current)-1]=total[int(current)-1]+1

                if i["survived"] == "1":
                    survive[int(current)-1] = survive[int(current)-1] + 1


        tempcount=0
        for j in total:
            result[tempcount]=survive[tempcount]/total[tempcount]*100
            tempcount=tempcount+1


        # under settings .
        data = [go.Bar(
            x=['First Class', 'Second Class', 'Third Class'],
            y=result
        )]
        layout = go.Layout(title="SurvialRateClassBarGraph", xaxis={'title': 'Class'},
                           yaxis={'title': 'Survival Rate %'})
        figure = go.Figure(data=data, layout=layout)
        py.plot(figure, filename='SurvialRateClassBarGraph')

        output = "First class survival rate: " + str(result[0]) + \
               "%" + "\n" + "Second class survival rate: " + str(result[1]) + "%"+\
                 "\n" + "Third class survival rate: " + str(result[2]) + "%"
        #Change this file path to your suite your computer
        f = open('/home/david/PycharmProjects/SimpleServer/static/ClassResult.txt', 'w')
        f.write(output)
        f.close()

        return render_template('ClassResult.html')

@app.route('/avgage/')
def AVG():
    # Assuming highest age on the boat was 120
    total = [0] * 3
    numClasses = [0] * 3
    result = [0] * 3

    for i in passengers:
        currentclass = i["class"]
        Class = int(currentclass)
        currentage= i["age"]
        if currentage == "":
            continue

        age = float(currentage)
        # adding to the array dependent on class
        total[Class-1] = total[Class-1] + age
        numClasses[Class-1]=numClasses[Class-1]+1
    tempcount = 0
    for j in total:
        result[tempcount] = total[tempcount] / numClasses[tempcount]
        tempcount = tempcount + 1
    data = [go.Bar(
        x=['First Class', 'Second Class', 'Third Class'],
        y=result
    )]
    layout = go.Layout(title="AverageAgePerClassBarGraph", xaxis={'title': 'Class'},
                       yaxis={'title': 'Average Age'})
    figure = go.Figure(data=data, layout=layout)
    py.plot(figure, filename='AverageAgePerClassBarGraph')

    output = "First class average age: " + str(result[0]) + \
             "\n" + "Second class average age: " + str(result[1]) + \
             "\n" + "Third class average age: " + str(result[2])
    # Change this file path to your suite your computer
    f = open('/home/david/PycharmProjects/SimpleServer/static/AVGAgeResult.txt', 'w')
    f.write(output)
    f.close()

    return render_template('AVGAge.html')









