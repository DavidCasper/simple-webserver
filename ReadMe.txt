Simple Web Server Readme.

This program was created using python 3.5

In order to run this program you must follow the following steps.

1)Install flask:

Type into terminal: pip install Flask

2)Install plotly:

Type into terminal : pip install plotly

3) Sign up for a plotly account:

Signup at the following URL : https://plot.ly/python/

4) Put your plotly credentials into the SimpleServer.py file:

Put them in on line 10 of the SimpleServer.py file:
py.sign_in('xxxxx', 'zk58x8ic99')  # Replace the username, and API key(which can be found under
#settings in your plotly profile) with your credentials

5) Running the program

Type the following commands into terminal to run the program:
1) export FLASK_APP=/home/david/PycharmProjects/SimpleServer/SimpleServer.py
   Note: The file path will be where you stored the file.

2) flask run

3) Copy and past your url into a web browser: http://127.0.0.1:5000/ (Note your url might be different)


6) Output:

   The output should be a plotly graph on one web page and text showing the result on another web page.

7) Problems

    If you have any problems please email me at: cspdav001@myuct.ac.za